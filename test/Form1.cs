﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using test.Model;
using System.IO;

namespace test
{
    public partial class FormMain : Form
    {
        private SSHFileTransfer sft;

        public FormMain()
        {
            InitializeComponent();
            sft = new SSHFileTransfer();
            labelToDirURI.Text = SSHFileTransfer.TO_DIR_PATH;
        }

        private void buttonSelectFile_Click(object sender, EventArgs e)
        {
            //if (openFileDialogMain.ShowDialog() == DialogResult.OK)
            if (folderBrowserDialogMain.ShowDialog() == DialogResult.OK)
            {
                labelFromDirURI.Text = folderBrowserDialogMain.SelectedPath;
                buttonTransfer.Enabled = true;
            }
        }

        private void buttonTransfer_Click(object sender, EventArgs e)
        {
            var b = sft.TranserFileIn(labelFromDirURI.Text);
            if (b)
            {
                MessageBox.Show("ファイルの転送に成功しました。");
                return;
            }
            MessageBox.Show("ファイルの転送に失敗しました…");
        }

        private void radioButtonFTP_CheckedChanged(object sender, EventArgs e)
        {
            var isChecked = radioButtonFTP.Checked;
            if (isChecked)
            {
                radioButtonReset();
                radioButtonFTP.Checked = isChecked;
            }
        }

        private void radioButtonSFTP_CheckedChanged(object sender, EventArgs e)
        {
            var isChecked = radioButtonSFTP.Checked;
            if (isChecked)
            {
                radioButtonReset();
                radioButtonSFTP.Checked = isChecked;
            }
        }

        private void radioButtonReset()
        {
            radioButtonSFTP.Checked = false;
            radioButtonFTP.Checked = false;
        }

        private void buttonExecComm_Click(object sender, EventArgs e)
        {
            var ssh = new SecureShell();
            var command = textBoxUnixCommand.Text;
            var result = ssh.ExecuteUnixCommand(command);
            MessageBox.Show(result);
        }
    }
}
