﻿namespace test
{
    partial class FormMain
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonTransfer = new System.Windows.Forms.Button();
            this.labelFromDir = new System.Windows.Forms.Label();
            this.openFileDialogMain = new System.Windows.Forms.OpenFileDialog();
            this.buttonSelect = new System.Windows.Forms.Button();
            this.folderBrowserDialogMain = new System.Windows.Forms.FolderBrowserDialog();
            this.labelToDir = new System.Windows.Forms.Label();
            this.labelToDirURI = new System.Windows.Forms.Label();
            this.labelTop = new System.Windows.Forms.Label();
            this.labelFromDirURI = new System.Windows.Forms.Label();
            this.buttonExecComm = new System.Windows.Forms.Button();
            this.radioButtonSFTP = new System.Windows.Forms.RadioButton();
            this.radioButtonFTP = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxUnixCommand = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonTransfer
            // 
            this.buttonTransfer.Enabled = false;
            this.buttonTransfer.Location = new System.Drawing.Point(198, 190);
            this.buttonTransfer.Name = "buttonTransfer";
            this.buttonTransfer.Size = new System.Drawing.Size(75, 23);
            this.buttonTransfer.TabIndex = 0;
            this.buttonTransfer.Text = "転送";
            this.buttonTransfer.UseVisualStyleBackColor = true;
            this.buttonTransfer.Click += new System.EventHandler(this.buttonTransfer_Click);
            // 
            // labelFromDir
            // 
            this.labelFromDir.AutoSize = true;
            this.labelFromDir.Location = new System.Drawing.Point(12, 96);
            this.labelFromDir.Name = "labelFromDir";
            this.labelFromDir.Size = new System.Drawing.Size(90, 12);
            this.labelFromDir.TabIndex = 3;
            this.labelFromDir.Text = "転送元ディレクトリ";
            // 
            // openFileDialogMain
            // 
            this.openFileDialogMain.FileName = "openFileDialog1";
            // 
            // buttonSelect
            // 
            this.buttonSelect.Location = new System.Drawing.Point(198, 129);
            this.buttonSelect.Name = "buttonSelect";
            this.buttonSelect.Size = new System.Drawing.Size(75, 23);
            this.buttonSelect.TabIndex = 6;
            this.buttonSelect.Text = "選択";
            this.buttonSelect.UseVisualStyleBackColor = true;
            this.buttonSelect.Click += new System.EventHandler(this.buttonSelectFile_Click);
            // 
            // labelToDir
            // 
            this.labelToDir.AutoSize = true;
            this.labelToDir.Location = new System.Drawing.Point(12, 159);
            this.labelToDir.Name = "labelToDir";
            this.labelToDir.Size = new System.Drawing.Size(90, 12);
            this.labelToDir.TabIndex = 9;
            this.labelToDir.Text = "転送先ディレクトリ";
            // 
            // labelToDirURI
            // 
            this.labelToDirURI.AutoSize = true;
            this.labelToDirURI.Location = new System.Drawing.Point(21, 180);
            this.labelToDirURI.Name = "labelToDirURI";
            this.labelToDirURI.Size = new System.Drawing.Size(52, 12);
            this.labelToDirURI.TabIndex = 10;
            this.labelToDirURI.Text = "ToDirURI";
            // 
            // labelTop
            // 
            this.labelTop.AutoSize = true;
            this.labelTop.BackColor = System.Drawing.Color.Navy;
            this.labelTop.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.labelTop.Location = new System.Drawing.Point(12, 9);
            this.labelTop.Name = "labelTop";
            this.labelTop.Padding = new System.Windows.Forms.Padding(3);
            this.labelTop.Size = new System.Drawing.Size(223, 18);
            this.labelTop.TabIndex = 0;
            this.labelTop.Text = "転送先ディレクトリにファイルを送るだけのアプリ";
            // 
            // labelFromDirURI
            // 
            this.labelFromDirURI.AutoSize = true;
            this.labelFromDirURI.Location = new System.Drawing.Point(21, 117);
            this.labelFromDirURI.Name = "labelFromDirURI";
            this.labelFromDirURI.Size = new System.Drawing.Size(65, 12);
            this.labelFromDirURI.TabIndex = 11;
            this.labelFromDirURI.Text = "FromDirURI";
            // 
            // buttonExecComm
            // 
            this.buttonExecComm.Location = new System.Drawing.Point(189, 241);
            this.buttonExecComm.Name = "buttonExecComm";
            this.buttonExecComm.Size = new System.Drawing.Size(84, 23);
            this.buttonExecComm.TabIndex = 13;
            this.buttonExecComm.Text = "実行！SSH！";
            this.buttonExecComm.UseVisualStyleBackColor = true;
            this.buttonExecComm.Click += new System.EventHandler(this.buttonExecComm_Click);
            // 
            // radioButtonSFTP
            // 
            this.radioButtonSFTP.AutoSize = true;
            this.radioButtonSFTP.Checked = true;
            this.radioButtonSFTP.Location = new System.Drawing.Point(11, 18);
            this.radioButtonSFTP.Name = "radioButtonSFTP";
            this.radioButtonSFTP.Size = new System.Drawing.Size(51, 16);
            this.radioButtonSFTP.TabIndex = 0;
            this.radioButtonSFTP.TabStop = true;
            this.radioButtonSFTP.Text = "SFTP";
            this.radioButtonSFTP.UseVisualStyleBackColor = true;
            this.radioButtonSFTP.CheckedChanged += new System.EventHandler(this.radioButtonSFTP_CheckedChanged);
            // 
            // radioButtonFTP
            // 
            this.radioButtonFTP.AutoSize = true;
            this.radioButtonFTP.Location = new System.Drawing.Point(135, 18);
            this.radioButtonFTP.Name = "radioButtonFTP";
            this.radioButtonFTP.Size = new System.Drawing.Size(44, 16);
            this.radioButtonFTP.TabIndex = 1;
            this.radioButtonFTP.Text = "FTP";
            this.radioButtonFTP.UseVisualStyleBackColor = true;
            this.radioButtonFTP.CheckedChanged += new System.EventHandler(this.radioButtonFTP_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonFTP);
            this.groupBox1.Controls.Add(this.radioButtonSFTP);
            this.groupBox1.Location = new System.Drawing.Point(12, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(261, 44);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // textBoxUnixCommand
            // 
            this.textBoxUnixCommand.BackColor = System.Drawing.SystemColors.WindowText;
            this.textBoxUnixCommand.ForeColor = System.Drawing.Color.Lime;
            this.textBoxUnixCommand.Location = new System.Drawing.Point(14, 219);
            this.textBoxUnixCommand.Name = "textBoxUnixCommand";
            this.textBoxUnixCommand.Size = new System.Drawing.Size(259, 19);
            this.textBoxUnixCommand.TabIndex = 14;
            this.textBoxUnixCommand.Text = "ls";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.textBoxUnixCommand);
            this.Controls.Add(this.buttonExecComm);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.labelFromDirURI);
            this.Controls.Add(this.labelTop);
            this.Controls.Add(this.labelToDirURI);
            this.Controls.Add(this.labelToDir);
            this.Controls.Add(this.buttonSelect);
            this.Controls.Add(this.labelFromDir);
            this.Controls.Add(this.buttonTransfer);
            this.Name = "FormMain";
            this.Text = "FileTransfer";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonTransfer;
        private System.Windows.Forms.Label labelFromDir;
        private System.Windows.Forms.OpenFileDialog openFileDialogMain;
        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogMain;
        private System.Windows.Forms.Label labelToDir;
        private System.Windows.Forms.Label labelToDirURI;
        private System.Windows.Forms.Label labelTop;
        private System.Windows.Forms.Label labelFromDirURI;
        private System.Windows.Forms.Button buttonExecComm;
        private System.Windows.Forms.RadioButton radioButtonSFTP;
        private System.Windows.Forms.RadioButton radioButtonFTP;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxUnixCommand;
    }
}

