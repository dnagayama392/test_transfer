﻿using System; /* Exception */
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tamir.SharpSsh; /* Scp, SshExec */
using Tamir.SharpSsh.jsch; /* JSch */
using System.Collections; /* Hashtable */
using Tamir.SharpSsh.java.io; /* InputStream */

namespace test.Model
{
    public class SecureShell
    {
        private const string HOST      = "192.168.5.7";
        private const string USER_NAME = "nagayama";
        private const string USER_PASS = "nagayama";
        private const int    PORT      = 22;
        private const string KEY_STRICT_HOST_KEY_CHECKING = "StrictHostKeyChecking";
        private const string VALUE_STRICT_HOST_KEY_CHECKING = "no";

        public SecureShell()
        {
        }

        public string ExecuteUnixCommand(string command)
        {
            var result = "";
            var ssh = new SshExec(HOST, USER_NAME, USER_PASS);
            try
            {
                ssh.Connect();
                result = ssh.RunCommand(command);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                ssh.Close();
            }
            return result;
        }

        public void SecureCopy()
        {
            var scp = new Scp(HOST, USER_NAME);
            scp.Password = USER_PASS;
            scp.Connect(PORT);
            scp.Put(@"C:\Data\MIKU.png", "/home/nagayama/miku.png");
            scp.Close();
        }
    }
}
