﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tamir.SharpSsh; /* Sftp */
using Tamir.SharpSsh.jsch; /* JSch, Session, ChannelSftp, SftpException */
using System.IO; /* Directory, FileInfo, Path, SearchOption */
using System.Collections; /* Hashtable */

namespace test.Model
{
    class SSHFileTransfer
    {
        private const string HOST      = "192.168.5.7"; /* yano-centOS */
        private const string USER_NAME = "nagayama";
        private const string USER_PASS = "nagayama";
        private const int    PORT      = 22;
        private const string KEY_STRICT_HOST_KEY_CHECKING   = "StrictHostKeyChecking";
        private const string VALUE_STRICT_HOST_KEY_CHECKING = "no";
        //private const string TO_DIR_PATH = "/home/nagayama/sftp/";
        //public const string TO_DIR_PATH = "/var/www/html/nagayama/MyLauncher/";
        //public const string TO_DIR_PATH = "/var/www/html/nagayama/MyLauncher/Application Files/MyLancher_1_0_0_7/";
        //public const string TO_DIR_PATH = "/var/www/html/nagayama/MvvmTestA/";
        public const string TO_DIR_PATH = "/var/www/html/nagayama/MvvmTestA/Application Files/MvvmTestA_1_0_0_0/";
        //public const string TO_DIR_PATH = "/var/www/html/nagayama/noused/";

        public SSHFileTransfer()
        {

        }

        /// <summary>
        /// 指定したディレクトリ内のファイルを転送します。
        /// </summary>
        /// <param name="fromDirPath">ディレクトリ名（フルパス）</param>
        /// <returns>成功判定(True:成功, False:失敗)</returns>
        public bool TranserFileIn(string fromDirPath)
        {
            var isSuccess = false;
            var filesAry = Directory.GetFiles(fromDirPath);
            foreach (var filepath in filesAry)
            {
                isSuccess = TranserFile(filepath);
                if (!isSuccess)
                {
                    return isSuccess;
                }
            }
            return isSuccess;
        }

        /// <summary>
        /// 指定したファイルを転送します。
        /// </summary>
        /// <param name="fromFilePath">ファイル名（フルパス）</param>
        /// <returns>成功判定(True:成功, False:失敗)</returns>
        public bool TranserFile(string fromFilePath)
        {
            var isSuccess = false;
            var toFilePath = TO_DIR_PATH + Path.GetFileName(fromFilePath);
            var count = 0;
            do
            {
                SecureCopy(fromFilePath, toFilePath);
                var size = GetSize(toFilePath);
                var localsize = GetLocalSize(fromFilePath);
                isSuccess = size.Equals(localsize);
                count++;
            }
            while (!isSuccess && count < 5);
            return isSuccess;
        }

        /// <summary>
        /// ローカルにあるファイルのサイズを取得
        /// </summary>
        /// <param name="fromFilePath">サイズを取得するファイル名（フルパス）</param>
        /// <returns>ファイルサイズ(long型)</returns>
        private long GetLocalSize(String fromFilePath)
        {
            long localFileSize = 0;
            var fileInfo = new FileInfo(fromFilePath);
            var size = fileInfo.Length;
            if (size > 0)
            {
                localFileSize = size;
            }
            return localFileSize;
        }

        /// <summary>
        /// 転送先にあるファイルのサイズを取得
        /// </summary>
        /// <param name="toFilePath">サイズを取得するファイル名（フルパス）</param>
        /// <returns>ファイルサイズ(long型)</returns>
        private long GetSize(String toFilePath)
        {
            long filesize = 0;
            var jsch = new JSch();
            // 公開鍵(KNOWN_HOST)を使う場合は以下のコメントアウトを解除
            //jsch.setKnownHosts(KNOWN_HOST);
            var session = jsch.getSession(USER_NAME, HOST, PORT);
            session.setPassword(USER_PASS);
            // 公開鍵を確認しないように設定
            var config = new Hashtable();
            config.Add(KEY_STRICT_HOST_KEY_CHECKING, VALUE_STRICT_HOST_KEY_CHECKING);
            session.setConfig(config);
            // セッション、チャンネルの接続
            session.connect();
            var channel = (ChannelSftp)session.openChannel("sftp");
            channel.connect();
            // ファイルサイズを取得
            try
            {
                SftpATTRS stat = channel.lstat(toFilePath);
                filesize = stat.getSize();
            }
            catch (SftpException ex)
            {
                System.Diagnostics.Debug.Assert(false, ex.Message);
            }
            finally
            {
                channel.disconnect();
                session.disconnect();
            }

            return filesize;
        }

        /// <summary>
        /// ファイルを転送先に複製（転送先ファイルが存在するならば上書き）
        /// </summary>
        /// <param name="fromFilePath">転送元ファイル名</param>
        /// <param name="toFilePath">転送先ファイル名</param>
        private void SecureCopy(String fromFilePath, String toFilePath)
        {
            var sftp = new Sftp(HOST, USER_NAME, USER_PASS);
            try
            {
                sftp.Connect(PORT);
                sftp.Put(fromFilePath, toFilePath);
            }
            catch (FileNotFoundException ex)
            {
                System.Diagnostics.Debug.Assert(false, ex.Message);
            }
            catch (SftpException ex)
            {
                System.Diagnostics.Debug.Assert(false, ex.Message);
            }
            finally
            {
                sftp.Close();
            }
        }
    }
}
